namespace smoothdev.K2.Blackpearl.ClientTools.Extensions {
	using System.Collections.Generic;
	using smoothdev.K2.Blackpearl.ClientTools.Wrappers;
	using SourceCode.SmartObjects.Client;

	/// <summary>
	/// Provides extension methods over <see cref="SmartParameterCollection"/>
	/// </summary>
	public static class SmartPropertyCollectionExtensions {
		internal static IEnumerable<string> GetKeys(this SmartPropertyCollection properties) {
			for (var i = 0; i < properties.Count; ++i)
				yield return properties[i].Name;
		}
		internal static IEnumerable<object> GetValues(this SmartPropertyCollection properties) {
			for (var i = 0; i < properties.Count; ++i)
				yield return properties[i].Value;
		}

		/// <summary>
		/// Wrap an instance of <see cref="SmartPropertyCollection"/> with an interface of type <typeparamref name="T"/> 
		/// </summary>
		/// <typeparam name="T">the interface type which serves as a wrapper</typeparam>
		/// <param name="smartPropertyCollection">data fields to be wrapped</param>
		/// <returns>an implementation of <typeparamref name="T"/> instance that wrap the <paramref name="smartPropertyCollection"/> instance</returns>
		/// <exception>will throw if T is not a public interface</exception>
		public static T GetAdapter<T>(this SmartPropertyCollection smartPropertyCollection) {
			return InjectableDependencies.DictionaryAdapterFactory.GetAdapter<T>(new SmartPropertyCollectionDictionaryWrapper(smartPropertyCollection));
		}
	}
}