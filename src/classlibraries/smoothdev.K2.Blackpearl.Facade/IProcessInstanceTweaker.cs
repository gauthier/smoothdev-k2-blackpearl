

namespace smoothdev.K2.Blackpearl.Facade
{
	using System;

	/// <summary>
	/// <para>provides an "internal" abstract access to perform mutation over SourceCode.Workflow.Client.ProcessInstance</para>
	/// <para>"internal" because it is meant be used only via <see cref="IProcessInstanceWrapper.Alter"/></para>
	/// </summary>
	public interface IProcessInstanceTweaker {
		IProcessInstanceTweaker setFolio(string name);
		IProcessInstanceTweaker operateOnDataFields<TDataFieldsWrapper>(Action<TDataFieldsWrapper> actionOnDataFields);
		IProcessInstanceTweaker setExpectedDuration(TimeSpan timespan);
	}
}