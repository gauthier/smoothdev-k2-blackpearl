

namespace smoothdev.K2.Blackpearl.Facade
{
	using System;

	/// <summary>
	/// provides bearable facade over SourceCode.Workflow.Client.ProcessInstance
	/// </summary>
	public interface IProcessInstanceWrapper {
		string Name { get; }
		string Description { get; }
		string Folder { get; }
		string FullName { get; }
		string Folio { get; }
		void Alter(Action<IProcessInstanceTweaker> performer);
	}
}