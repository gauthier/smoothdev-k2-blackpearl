namespace smoothdev.K2.Blackpearl.ClientTools.Extensions {
	using System;
	using SourceCode.Workflow.Client;
	using Wrappers;
	/// <summary>
	/// Provides extension methods over <see cref="Connection"/>
	/// </summary>
	public static class ConnectionExtensions {
		/// <summary>
		/// <para>when called, connection is impersonated to the given userIdentifier</para>
		/// <para>connection is impersonated to previous user on disposal</para>
		/// <remarks>will do nothing if Connection.User.Name is null or empty</remarks>
		/// </summary>
		/// <param name="connection">client connection, should have User.Name defined</param>
		/// <param name="userIdentifier">user identifier to impersonate connection</param>
		/// <returns>Returns a disposable scope for impersonation</returns>
		public static IDisposable ScopedImpersonation(this Connection connection, string userIdentifier) {
			return new UserImpersonationClientConnectionScope(connection, userIdentifier);
		}
	}
}