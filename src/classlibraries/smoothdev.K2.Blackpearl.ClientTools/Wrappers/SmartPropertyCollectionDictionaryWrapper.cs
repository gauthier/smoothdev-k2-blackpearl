namespace smoothdev.K2.Blackpearl.ClientTools.Wrappers {
	using System;
	using System.Collections.Generic;
	using smoothdev.K2.Blackpearl.ClientTools.Extensions;
	using SourceCode.SmartObjects.Client;

	internal class SmartPropertyCollectionDictionaryWrapper : BaseDictionaryWrapper<SmartPropertyCollection> {
		internal SmartPropertyCollectionDictionaryWrapper(SmartPropertyCollection smartPropertyCollection) : base(smartPropertyCollection) { }
		protected internal override Func<IEnumerable<string>> keysIterator { get { return wrappedInstance.GetKeys; } }
		protected internal override Func<IEnumerable<object>> valuesIterator { get { return wrappedInstance.GetValues; } }
		protected internal override Func<string, object> valueForKey { get { return (k) => wrappedInstance[k].Value; } }
		protected internal override Action<string, object> valueForKeySetter { get { return (k, v) => wrappedInstance[k].Value = v.ToString(); } }
		protected internal override Func<int, object> valueAtIndexGetter { get { return (i) => wrappedInstance[i].Value; } }
		protected internal override Action<int, object> valueAtIndexSetter { get { return (i, v) => wrappedInstance[i].Value = v.ToString(); } }
		protected internal override Func<int, string> keyAtIndex { get { return (i) => wrappedInstance[i].Name; } }
		protected internal override Func<int> getCount { get { return () => wrappedInstance.Count; } }
	}
}