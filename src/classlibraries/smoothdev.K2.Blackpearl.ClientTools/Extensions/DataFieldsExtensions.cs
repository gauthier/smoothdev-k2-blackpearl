namespace smoothdev.K2.Blackpearl.ClientTools.Extensions {
	using System.Collections.Generic;
	using smoothdev.K2.Blackpearl.ClientTools.Wrappers;
	using SourceCode.Workflow.Client;
	/// <summary>
	/// Provides extension methods over <see cref="DataFields"/>
	/// </summary>
	public static class DataFieldsExtensions {
		internal static IEnumerable<string> GetKeys(this DataFields dataFields) {
			for (var i = 0; i < dataFields.Count; ++i)
				yield return dataFields[i].Name;
		}
		internal static IEnumerable<object> GetValues(this DataFields dataFields) {
			for (var i = 0; i < dataFields.Count; ++i)
				yield return dataFields[i].Value;
		}

		/// <summary>
		/// Wrap an instance of <see cref="DataFields"/> with an interface of type <typeparamref name="T"/> 
		/// </summary>
		/// <typeparam name="T">the interface type which serves as a wrapper</typeparam>
		/// <param name="dataFields">data fields to be wrapped</param>
		/// <returns>an implementation of <typeparamref name="T"/> instance that wrap the <paramref name="dataFields"/> instance</returns>
		/// <exception>will throw if T is not a public interface</exception>
		public static T GetAdapter<T>(this DataFields dataFields) {
			return InjectableDependencies.DictionaryAdapterFactory.GetAdapter<T>(new DataFieldsDictionaryWrapper(dataFields));
		}
	}
}