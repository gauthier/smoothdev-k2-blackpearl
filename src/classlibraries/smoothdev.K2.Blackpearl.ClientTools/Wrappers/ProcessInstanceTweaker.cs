namespace smoothdev.K2.Blackpearl.ClientTools
{
	using System;
	using smoothdev.K2.Blackpearl.ClientTools.Extensions;
	using SourceCode.Workflow.Client;
	using smoothdev.K2.Blackpearl.Facade;

	internal class ProcessInstanceTweaker : IProcessInstanceTweaker {
		private ProcessInstance processInstance;
		/*public ProcessInstanceTweaker(Guid processGuid){}
		public ProcessInstanceTweaker(int processId){}*/
		public ProcessInstanceTweaker(ProcessInstance processInstance) {
			this.processInstance = processInstance;
		}
		public IProcessInstanceTweaker setFolio(string name) {
			processInstance.Folio = name;
			return this;
		}
		public IProcessInstanceTweaker operateOnDataFields<TDataFieldsWrapper>(Action<TDataFieldsWrapper> actionOnDataFields) {
			actionOnDataFields(processInstance.DataFields.GetAdapter<TDataFieldsWrapper>());
			return this;
		}
		public IProcessInstanceTweaker setExpectedDuration(TimeSpan timespan) {
			// go figure if it is second without documentation?
			processInstance.ExpectedDuration = (int)timespan.TotalSeconds;
			return this;
		}
	}
}