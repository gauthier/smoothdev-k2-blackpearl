namespace smoothdev.K2.Blackpearl.ClientTools
{
	using System;
	using smoothdev.K2.Blackpearl.Facade;
	using SourceCode.Workflow.Client;
	public class ProcessInstanceWrapper : IProcessInstanceWrapper
	{
		public static IProcessInstanceWrapper Create(ProcessInstance processInstance)
		{
			return new ProcessInstanceWrapper{processInstance = processInstance};
		}

		private ProcessInstance processInstance { get; set; }
		public string Name
		{
			get { return processInstance.Name; }
		}

		public string Description
		{
			get { return processInstance.Name; }
		}

		public string Folder
		{
			get { return processInstance.Name; }
		}

		public string FullName
		{
			get { return processInstance.Name; }
		}

		public string Folio
		{
			get { return processInstance.Name; }
		}

		public void Alter(Action<IProcessInstanceTweaker> performer)
		{
			performer(new ProcessInstanceTweaker(processInstance));
		}


	}
}