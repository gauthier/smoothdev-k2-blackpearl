
namespace smoothdev.K2.Blackpearl.ClientTools {
	using System;
	using Castle.Components.DictionaryAdapter;


	/// <summary>
	/// Here is provided entry point to inject dependencies
	/// </summary>
	public static class InjectableDependencies {
		/// <summary>
		/// default instance of dictionary adapter factory in case it's not injected otherwise
		/// </summary>
		internal static IDictionaryAdapterFactory dictionaryAdapterFactory = new DictionaryAdapterFactory();

		/// <summary>
		/// <para>Provides access to singleton instance of dictionary adapter factory.</para>
		/// <para>By default, an initial instance is made available.</para>
		/// <para>Use the setter to provide your own instance (there should only be one instance by process).</para>
		/// </summary>
		public static IDictionaryAdapterFactory DictionaryAdapterFactory { get { return dictionaryAdapterFactory; } set { dictionaryAdapterFactory = value; } }
	}

	internal static class Guard {
		internal static void AgainstArgumentNotNull<T>(T argumentValue, string argumentName) {
			if (argumentValue == null)
				throw new ArgumentNullException(argumentName);
		}
	}
}